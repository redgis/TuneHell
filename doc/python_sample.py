import base64

def C2STransform(buffer):
	# we always consume the entire buffer
	bytesConsumed = len(buffer)
	outputBuffer = base64.b64encode(buffer)
	# print(outputBuffer)
	return [outputBuffer, bytesConsumed]

def C2SUnTransform(buffer):
	bytesConsumed = 0
	try:
		outputBuffer = base64.b64decode(buffer)
	except binascii.Error:
		bytesConsumed = 0
		return [outputBuffer, bytesConsumed]
		
	#How many bytes from input buffer where decoded ? These are the consumed bytes
	padding1Pos = buffer.find(b'=')
	padding2Pos = buffer.find(b'==')
	
	endOfBuf1 = len(buffer)
	endOfBuf2 = len(buffer)
	
	if padding1Pos != -1:
		endOfBuf1 = padding1Pos + 1
		
	if padding2Pos != -1:
		endOfBuf2 = padding2Pos + 2
		
	if padding2Pos <= padding1Pos:
		bytesConsumed = endOfBuf2
	else:
		bytesConsumed = endOfBuf1
		
	# print('Input: ', buffer, len(buffer))
	# print('Consumed: ', buffer[0:bytesConsumed])
	# print('result: ', outputBuffer)
	return [outputBuffer, bytesConsumed]
	
# S2CTransform
# S2CUnTransform





# def C2STransform(buffer):
	# global bytesConsumed
	# bytesConsumed = len(buffer)
	# return {buffer, len(buffer)}

# def C2SUnTransform(buffer):
	# global bytesConsumed
	# bytesConsumed = len(buffer)
	# return {buffer, len(buffer)}
	
# C2STransform(b'dlfkhdlkghdflghdlgsqmdlgjmsdkgjlkhglksdlghhg')
# C2SUnTransform(b'ZGxma2hkbGtnaGRmbGdoZGxnc3FtZGxnam1zZGtnamxraGdsa3NkbGdoaGc=ZGxma2hkbGtnaGRmbGdoZGxnc3FtZGxnam1zZGtnamxraGdsa3NkbGdoaGc=')