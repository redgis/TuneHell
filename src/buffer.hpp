#pragma once

namespace TuneHell
{

   /** Simple buffer management class. This class is used to handle buffers of data, allowing
    *    insertion, deletion, with underlying automatic reallocation.
    */
   class CBuffer
   {
      private:
         unsigned char * m_buffer;  /**< Buffer address */
         std::size_t m_capacity;    /**< Buffer current capacity */
         std::size_t m_offset;      /**< Offset at which data begins */
         std::size_t m_size;        /**< Data size */


         void zero();
         void reallocate(std::size_t capacity);
         void grow(std::size_t atleast);

      public:

         /** Structure allowing ownership transfert to foreign code.
          */
         typedef struct sYieldBuffer {
            unsigned char* buffer;  /**< Buffer address */
            std::size_t capacity;   /**< Buffer current capacity */
            std::size_t offset;     /**< Offset at which data begins */
            std::size_t size;       /**< Data size */
         } tYieldBuffer;

         CBuffer();
         CBuffer(unsigned char* buffer, std::size_t size, std::size_t capacity,
            std::size_t offset);
         CBuffer(std::size_t capacity, std::size_t offset);
         CBuffer(tYieldBuffer &buf);
         CBuffer(unsigned char * m_buffer, std::size_t m_size);
         CBuffer(char *szString);
         CBuffer(std::string & stdString);
         CBuffer(CBuffer &&);
         ~CBuffer();

         //Forbid implicit.
         CBuffer(CBuffer &) = delete;
         CBuffer(CBuffer const&) = delete;
         CBuffer & operator=(CBuffer &) = delete;
         CBuffer & operator=(CBuffer const &) = delete;

         //Make copy explicit
         void copy(CBuffer & inputBuf);
         void copy(CBuffer::tYieldBuffer & inputBuf);

         bool isAttached();
         void attach(tYieldBuffer &buf);
         tYieldBuffer detach();

         //Free the allocated memory.
         void discard();

         void reserve(std::size_t capacity);
         std::size_t capacity();
         std::size_t size();
         const unsigned char * data();

         unsigned char &operator[] (std::size_t);
         CBuffer & operator=(CBuffer && buf);

         void push_front(const unsigned char * m_buffer, std::size_t m_size);
         void push_front(const char *szString);
         void push_front(const std::string & stdString);
         void push_front(CBuffer & buffer);

         void push_back(const unsigned char * m_buffer, std::size_t m_size);
         void push_back(const char * szString);
         void push_back(const std::string & stdString);
         void push_back(CBuffer & buffer);

         void erase(const std::size_t offset, const std::size_t count);
         void insert(const std::size_t offset, unsigned char * data, const std::size_t count);

         //Clear data, but keep allocated memory
         void clear();
         //Remove count bytes at the front
         void pop_front(std::size_t count);
         //Remove count bytes from the end
         void pop_back(std::size_t count);
         
         /** Buffer iterator class.
          */
         class iterator : public std::iterator<std::bidirectional_iterator_tag, unsigned char> {
            private:
               unsigned char * m_pByte;
            public:
               iterator(unsigned char *x) : m_pByte(x) {}
               iterator(const iterator& mit) : m_pByte(mit.m_pByte) {}
               iterator& operator++() { ++m_pByte; return *this; }
               iterator operator++(int) { iterator tmp(*this); operator++(); return tmp; }
               iterator& operator--() { --m_pByte; return *this; }
               iterator operator--(int) { iterator tmp(*this); operator--(); return tmp; }
               bool operator==(const iterator& rhs) { return m_pByte == rhs.m_pByte; }
               bool operator!=(const iterator& rhs) { return m_pByte != rhs.m_pByte; }
               unsigned char& operator*() { 
                  return *m_pByte; 
               }

         };

         iterator begin();
         iterator end();

         std::size_t rightSpaceRemaining();
         std::size_t leftSpaceRemaining();

         void resetOffset(std::size_t newOffset);
   };

} //TuneHell namespace