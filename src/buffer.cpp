#include "TuneHell.hpp"
#include "buffer.hpp"


namespace TuneHell
{


   /** Zeros the buffer.
    */
   void CBuffer::zero() {
      this->m_buffer = nullptr;
      this->m_offset = this->m_capacity = m_size = 0;
   }

   /** Constructs an empty unallocated buffer.
    */
   CBuffer::CBuffer() {
      this->zero();
   }

   /** Allocates a new buffer with a specified capacity and data offset.
    *  \param capacity capacity of the buffer
    *  \param offset buffer offset at which data begins (i.e. will be inserted using 
    *    CBuffer::push_front or CBuffer::push_back)
    */
   CBuffer::CBuffer(std::size_t capacity, std::size_t offset)
   {
      this->m_buffer = new unsigned char[capacity];
      this->m_capacity = capacity;
      this->m_offset = offset;
      this->m_size = 0;
   }

   /** Allocates a new buffer and copies provided data in it.
    *  \param buffer buffer containing data to be copied in the newly allocated buffer
    *  \param size size of the data provided
    */
   CBuffer::CBuffer(unsigned char * buffer, std::size_t size) {
      this->zero();
      this->push_back(buffer, size);
   }

   /** Allocates a new buffer and copies provided data in it.
    *  \param szString null-terminated string provided as data for the newly allocated buffer
    */
   CBuffer::CBuffer(char *szString) {
      this->zero();
      this->push_back(szString);
   }

   /** Allocates a new buffer and copies provided data in it.
    *  \param stdString string provided as data for the newly allocated buffer
    */
   CBuffer::CBuffer(std::string & stdString) {
      this->zero();
      this->push_back(stdString);
   }

   /** Move constructor.
    *  \param buf buffer to be moved to current instance
    */
   CBuffer::CBuffer(CBuffer && buf) {
      this->zero();
      this->m_buffer = buf.m_buffer;
      this->m_capacity = buf.m_capacity;
      this->m_offset = buf.m_offset;
      this->m_size = buf.m_size;
      buf.zero();
   }

   /** Initializes a buffer with a specified buffer address, capacity, data offset and size.
    *  \param buffer buffer address to be used
    *  \param capacity capacity of the buffer
    *  \param offset buffer offset at which data begins (i.e. will be inserted using 
    *    CBuffer::push_front() or CBuffer::push_back())
    *  \param size size of the data
    */
   CBuffer::CBuffer(unsigned char* buffer, std::size_t size, std::size_t capacity,
      std::size_t offset) 
   {
      this->m_buffer = buffer;
      this->m_capacity = capacity;
      this->m_offset = offset;
      this->m_size = size;
   }

   /** Initializes a buffer with a specified buffer address, capacity, data offset and size. The 
    *    new buffer takes ownership of the buffer described by \a buf, and zeros \a buf.
    *  \param buf CBuffer::tYieldBuffer object containing buffer address, capacity, 
    *    data offset and data size to be used.
    */
   CBuffer::CBuffer(CBuffer::tYieldBuffer &buf) {
      this->m_buffer = buf.buffer;
      this->m_capacity = buf.capacity;
      this->m_offset = buf.offset;
      this->m_size = buf.size;
      buf = { nullptr, 0, 0, 0 };
   }

   /** Destructs buffer, deallocated any previously allocated memory buffer.
    */
   CBuffer::~CBuffer() {
      this->discard();
   }


   /** Copies the data from input buffer to current buffer. Any previously contained data in the 
    *    current buffer, is lost.
    *  \param inputBuf buffer from which to copy data.
    */
   void CBuffer::copy(CBuffer & inputBuf) {
      tYieldBuffer tmpBuf = inputBuf.detach();
      this->copy(tmpBuf);
      inputBuf.attach(tmpBuf);
   }

   /** Copies the data from input buffer to current buffer. Any previously contained data in the 
    *    current buffer, is lost.
    *  \param inputBuf buffer from which to copy data.
    */
   void CBuffer::copy(CBuffer::tYieldBuffer & inputBuf) {
      this->reserve(inputBuf.capacity);
      this->m_offset = inputBuf.offset;
      this->m_size = inputBuf.size;
      std::memcpy(this->m_buffer + this->m_offset, inputBuf.buffer+inputBuf.offset, inputBuf.size);
   }


   /** Deallocates if previously allocated and resets the buffer.
    */
   void CBuffer::discard() {
      delete this->m_buffer;
      this->zero();
   }

   /** Check if this instance has ownership of a buffer.
    *  \return \a true if has a buffer, \a false otherwise
    */
   bool CBuffer::isAttached() {
      return (this->m_buffer != nullptr);
   }

   /** Discards currently owned buffer and takes ownership of provided buffer \a buf.
    *  \param buf description of buffer from which to take ownership
    */
   void CBuffer::attach(CBuffer::tYieldBuffer &buf)
   {
      this->discard();
      this->m_buffer = buf.buffer;
      this->m_capacity = buf.capacity;
      this->m_offset = buf.offset;
      this->m_size = buf.size;
      buf = {nullptr, 0, 0, 0};
   }

   /** Yield ownership of currently owned buffer. The current instance is then reset.
    *  \return the previously owned buffer yielded as a CBuffer::tYieldBuffer object
    */
   CBuffer::tYieldBuffer CBuffer::detach() {
      CBuffer::tYieldBuffer result = 
         { this->m_buffer, this->m_capacity, this->m_offset, this->m_size };
      this->zero();

      return result;
   }
   
   /** Reallocates (or allocates if not allocated yet) to fit with \a newcapacity. The new 
    *    capacity will be (newcapacity - this->size()) / 2;
    *  \param newcapacity new capacity needed
    */
   void CBuffer::reallocate(std::size_t newcapacity) {
      assert(newcapacity >= this->m_size);

      unsigned char *newbuffer = new unsigned char[newcapacity];
      std::size_t newoffset = (newcapacity - this->m_size) / 2;

      std::memcpy(newbuffer + newoffset, 
         this->m_buffer + this->m_offset, 
         this->m_size);
      
      delete this->m_buffer;
      this->m_buffer = newbuffer;
      this->m_offset = newoffset;
      this->m_capacity = newcapacity;
   }


   /** Increase capacity of the buffer so that it can contain at least \a atleast size. If the 
    *    current capacity is not enough it will be by \a capacity / 2.
    *  \param atleast data size needed
    */
   void CBuffer::grow(std::size_t atleast) {
      std::size_t newcapacity = 
         this->m_capacity + this->m_capacity / 2;

      newcapacity = std::max(atleast, newcapacity);
      this->reallocate(newcapacity);
   }

   /** Current capacity of the buffer.
    *  \return current capacity of the buffer
    */
   std::size_t CBuffer::capacity() {
      return this->m_capacity;
   }

   /** Current size of the buffer.
    *  \return current size of the buffer
    */
   std::size_t CBuffer::size() {
      return this->m_size;
   }

   /** Allocates the buffer with capacity. The reallocation is only performed if the current 
    *    capacity if smaller then \a capacity.
    *  \param capacity new capacity wanted
    */
   void CBuffer::reserve(std::size_t capacity) {
      if (this->m_capacity < capacity)
         this->reallocate(capacity);
   }

   /** Retrieves the address of the data stored in the buffer. This doesn't change the ownership 
    *    of this instance. The returned data shouldn't be modified.
    *  \return address of the data stored in the buffer
    */
   const unsigned char * CBuffer::data() {
      return this->m_buffer + this->m_offset;
   }

   /** Random access operator. Retrieves a reference to the data at a specified index.
    *  \param index index of the byte to be retrieved
    *  \return reference to the byte at specified index
    */
   unsigned char &CBuffer::operator[](std::size_t index) {
      assert(index < this->m_size);
      return *(this->m_buffer + this->m_offset + index);
   }

   /** \a move assignation operator. Abandon previous ownership by releasing the buffer, takes 
    *    ownership of the passed buffer.
    *  \param buf buffer to use as new owned buffer
    */
   CBuffer & CBuffer::operator=(CBuffer && buf) {
      this->discard();
      this->m_buffer = buf.m_buffer;
      this->m_capacity = buf.m_capacity;
      this->m_offset = buf.m_offset;
      this->m_size = buf.m_size;
      buf.zero();
      return *this;
   }

   /** Inserts bytes before the already stored data.
    *  \param buffer data to be inserted
    *  \param size the size of the data to be inserted
    */
   void CBuffer::push_front(const unsigned char * buffer, std::size_t size) {
      if (size > this->m_offset)
         this->reallocate(this->m_capacity + 4 * size);

      std::memcpy(this->m_buffer + this->m_offset - size,
         buffer, size);
      this->m_offset -= size;
      this->m_size += size;
   }

   /** Inserts bytes before the already stored data.
   *  \param szString null-terminated string to be inserted
   */
   void CBuffer::push_front(const char *szString) {
      this->push_front((unsigned char *)szString, std::strlen(szString));
   }

   /** Inserts bytes before the already stored data.
    *  \param stdString string to be inserted
    */
   void CBuffer::push_front(const std::string &stdString) {
      this->push_front((unsigned char*)stdString.data(), stdString.length());
   }

   /** Inserts bytes before the already stored data.
    *  \param buffer buffer from which data is to be copied
    */
   void CBuffer::push_front(CBuffer &buffer) {
      this->push_front(buffer.data(), buffer.size());
   }

   /** Removes bytes at the begining of the data.
    * \param count number of bytes to be removed
    */
   void CBuffer::pop_front(const std::size_t count) {
      assert(count <= this->m_size);
      this->m_size -= count;
      this->m_offset += count;
   }

   /** Inserts bytes after the already stored data.
    *  \param buffer data to be inserted
    *  \param size the size of the data to be inserted
    */
   void CBuffer::push_back(const unsigned char * buffer, std::size_t size) {
      if (size > this->m_capacity - 
         (this->m_offset + this->m_size))
      {
         this->reallocate(this->m_capacity + 4 * size);
      }

      std::memcpy(this->m_buffer + this->m_offset + this->m_size,
         buffer, size);
      this->m_size += size;
   }

   /** Inserts bytes after the already stored data.
    *  \param szString null-terminated string to be inserted
    */
   void CBuffer::push_back(const char * szString) {
      this->push_back((unsigned char*)szString, std::strlen(szString));
   }

   /** Inserts bytes after the already stored data.
    *  \param stdString string to be inserted
    */
   void CBuffer::push_back(const std::string & stdString) {
      this->push_back((unsigned char*)stdString.data(), stdString.length());
   }

   /** Inserts bytes after the already stored data.
    *  \param buffer buffer from which data is to be copied
    */
   void CBuffer::push_back(CBuffer &buffer) {
      this->push_back(buffer.data(), buffer.size());
   }

   /** Removed bytes from the end of the data.
    * \param count number of bytes to be removed
    */
   void CBuffer::pop_back(const std::size_t count) {
      assert(count <= this->m_size);
      this->m_size -= count;
   }

   /** Removed bytes from the data.
    * \param offset offset at which to remove the bytes
    * \param count number of bytes to be removed
    */
   void CBuffer::erase(const std::size_t offset, const std::size_t count) {
      assert(offset >= 0 && offset <= this->m_size);
      assert(count <= this->m_size - offset);

      if (offset == 0) {
         this->pop_front(count);
         return;
      }

      if (offset == this->m_size) {
         this->pop_back(count);
         return;
      }

      //memmove handles the possibility of source and destination overlapping
      std::memmove(this->m_buffer + this->m_offset + offset, 
         this->m_buffer + this->m_offset + offset + count,
         this->m_size - (offset + count) );

      this->m_size -= count;
   }

   /** Insert bytes in the buffer.
    * \param offset offset at which to insert the bytes
    * \param data data buffer from which to copy data to insert
    * \param count number of bytes to be inserted
    */
   void CBuffer::insert(const std::size_t offset, unsigned char * data, const std::size_t count) {
      assert(offset >= 0 && offset <= this->m_size);
      assert(data != nullptr);

      if (offset == 0) {
         this->push_front(data, count);
         return;
      }

      if (offset == this->m_size) {
         this->push_back(data, count);
         return;
      }


      if (count > this->m_capacity - (this->m_offset + this->m_size) )
      {
         this->reallocate(this->m_capacity + 4 * count);
      }

      //shift data that is after the given offset
      //(memmove handles the possibility of source and destination overlapping)
      std::memmove(this->m_buffer + this->m_offset + offset + count,
         this->m_buffer + this->m_offset + offset,
         this->m_size - (this->m_offset + offset));

      //copy the input data
      std::memcpy(this->m_buffer + this->m_offset + offset, data, count);
   }

   /** Clear the data stored in the buffer. The buffer allocation remains un changed.
    */
   void CBuffer::clear() {
      this->m_size = 0;
      this->m_offset = this->m_capacity / 5;
   }

   /** Provides an iterator positionned on the first element of the buffer
    *  \return Iterator positionned on the first elemnt of the buffer
    */
   CBuffer::iterator CBuffer::begin() {
      return CBuffer::iterator(this->m_buffer + this->m_offset);
   }

   /** Provides an iterator positionned on the element after the last element of the buffer.
    *  \return Iterator positionned on the element after the last element of the buffer
    */
   CBuffer::iterator CBuffer::end() {
      return CBuffer::iterator(this->m_buffer + this->m_offset
         + this->m_size);
   }

   /** Indicates buffer space left on the right-most end
    *  \return space left on the right-most end
    */
   std::size_t CBuffer::rightSpaceRemaining() {
      return this->m_capacity - (this->m_offset + this->m_size);
   }

   /** Indicates buffer space left on the left-most end
    *  \return space left on the left-most end
    */
   std::size_t CBuffer::leftSpaceRemaining() {
      return this->m_offset;
   }

   /** Reset offet position in the buffer, to a given value. This can implie a reallocation.
    *  \param newOffset the new offset value
    */
   void CBuffer::resetOffset(std::size_t newOffset) {
      assert(newOffset >= 0 && newOffset < this->m_capacity);

      if (newOffset + this->m_size > this->m_capacity) {
         this->grow(newOffset + this->m_size);
      }

      std::memmove(this->m_buffer + newOffset, this->m_buffer + this->m_offset, this->m_size);
      this->m_offset = newOffset;
   }

} //TuneHell namespace