#include "TuneHell.hpp"


/** Simple string spliting funcntion.
 *  \param str input string to be split
 *  \param sep seperator charactor used to split the input string
 *  \param result in-out vector of strings containing the split result
 */
void split(std::string str, char sep, std::vector<std::string> &result) {
   
   result.clear();

   auto start = std::begin(str);
   auto strEnd = std::end(str);
   auto end = start;

   do {

      while (start != strEnd && *start == sep)
         start++;

      end = start;

      while (end != strEnd && *end != sep)
         end++;

      if (start != end) {
         result.emplace_back(start, end);
         start = end;
      }

   } while (end != strEnd && start != strEnd);

}

/** Parses a string for being a supported datatransformation. This function is used when parsing 
 *    command-line defined redirections.
 *  \param str string denoting a supported data transformation
 *  \return TuneHell::CDataTransform object representing the recognized data transformation, or 
 *    nullptr if failed to recognized any supported data transformation
 */
std::unique_ptr<TuneHell::CDataTransform> getTransformation(std::string str) {
   TuneHell::CDataTransform *transformation = nullptr;

   bool reverseTransformation = false;

   if (str[0] != '-' && str[0] != '+') {
      return nullptr;
   }

   if (str[0] == '-') {
      reverseTransformation = true;
   }

   str = str.substr(1);


   if (str == "raw") {
      transformation = nullptr;
   } else if (str.substr(0, 7) == "caesar(") {
      unsigned char key = std::stoi(str.substr(7, str.length() - (7+1))) % 255;
      transformation = new TuneHell::CCaesar(key);
   } else if (str.substr(0, 9) == "vigenere(") {
      std::string key = str.substr(9, str.length() - (9+1));
      transformation = new TuneHell::CVigenere((unsigned char *)key.c_str(), key.length());
   } else if (str == "html") {
      transformation = new TuneHell::CHtml();
   } else if (str == "base64") {
      transformation = new TuneHell::CBase64();
   } else if (str == "http") {
      transformation = new TuneHell::CHttp();
   } else if (str.substr(0, 7) == "python(") {
      transformation = new TuneHell::CPython(str.substr(7, str.length() - (7 + 1)));
   } else if (str.substr(0, 7) == "aes(") {
      transformation = new TuneHell::CSymetricCrypt<CryptoPP::AES>(str.substr(4, str.length() - (4 + 1)));
   } else if (str.substr(0, 7) == "trippledes(") {
      transformation = new TuneHell::CSymetricCrypt<CryptoPP::DES_EDE2>(str.substr(11, str.length() - (11 + 1)));
   } else {
      std::cerr << "Error, transformation unknown or not implemented (yet?): \""
         << str << "\"." << std::endl;
      transformation = nullptr;
   }


   if (transformation != nullptr) {
      transformation->setReverseTransformation(reverseTransformation);
   }

   return std::unique_ptr<TuneHell::CDataTransform> (transformation);
}

/** Parse the various command-line defined redirections.
 *  \param redirStr command-line argument describing a redirection, in the form of 
 *    [local port];[remote host];[remote port];{comma-separated list of transformations}
 *  \return TuneHell::CTunnel object representing the redirection, or nullptr if
 *    failed to parse the command-line argument
 */
TuneHell::CTunnel * parseRedirection(std::string redirStr) {

   std::vector<std::string> params;
   split(redirStr, ';', params);

   if (params.size() < 3 || params.size() > 4) {
      std::cerr << "Error reading parameter \"" << redirStr << "\"" << std::endl;
      std::cerr << "   Expected 3 or 4 ':' seperated items." << std::endl;
      return nullptr;
   }

   unsigned short localPort = static_cast<unsigned short>(std::atoi(params[0].c_str()));
   std::string targetHost = params[1];
   unsigned short remotePort = static_cast<unsigned short>(std::atoi(params[2].c_str()));
   
   TuneHell::CTunnel * newTunnel = new TuneHell::CTunnel(localPort, targetHost, remotePort);

   // Data transformation provided
   if (params.size() == 4) {
      std::vector<std::string> transforms;
      split(params[3], ',', transforms);

      for (auto transform : transforms) {
         std::unique_ptr<TuneHell::CDataTransform> transformation =  getTransformation(transform);
         if (transformation != nullptr) {
            newTunnel->addTransformation(std::move(transformation));
         }
      }
   }

   return newTunnel;
}

/** Parse command-line arguments.
 *  \param argc count of command-line arguments
 *  \param argv command-line arguments recieved from main function
 *  \param tunnels list that will recieve the redirections defined on the command-line
 */
void parseArgs(int argc, char *argv[], std::list< std::unique_ptr<TuneHell::CTunnel> > & tunnels) {

   std::vector<std::string> redirections;

   boost::program_options::options_description paramDesc("Parameters");
   paramDesc.add_options()
      ("help,h", "Adiuvare ergo sum")

      ("verbose,v", "Display connections activity output")

      ("log,l", "Write detailed debug output to file. Slow !")

      ("redirection,r",
         boost::program_options::value<std::vector<std::string> >(&redirections),
         "space separated list of redirections defined as follows: \n"
         "[local port];[remote host];[remote port];{comma-separated list of transformations}\n\n"
         "Data transformations currently supported : \n"
         "  html \n"
         "  caesar(key) : key is a integer < 255 \n"
         "  vigenere(key) : key is a string \n"
         "  base64 \n"
         "  http \n"
         "  python(scriptname) \n"
         "  aes(keyfile) \n"
         "  trippledes(keyfile) \n"
         "  https* \n"
         "  dns* \n"
         "  des(keyfile)* \n"
         "  rsa(keyfile)* \n"
         "  \n"
         "Transformation is defined from client to server, and must be prefixed with '+' for "
         "normal transformation, '-' for reverse transformation.\n"
         "* are not implemented yet")

      ("config-file,c",
         "load parameters from a JSON config file");


   boost::program_options::variables_map varMap;
   
   try
   {
      boost::program_options::store(
         boost::program_options::parse_command_line(argc, argv, paramDesc),
         varMap
      );

      boost::program_options::notify(varMap);

      if (varMap.count("verbose")) {
         TuneHell::verbose_mode = true;
      }

      if (varMap.count("log")) {
         TuneHell::log_mode = true;
      }
      
      if (varMap.count("help") || 
          varMap.count("redirection") == 0 && varMap.count("config-file") == 0)
      {
         std::cout << "This program allows one to redirect TCP communications from local port" 
            << std::endl;
         std::cout << "to remote host/port. Data transformations can be applied when data is"
            << std::endl;
         std::cout << "recieved and before being forwarded." << std::endl << std::endl;

         std::cout << paramDesc << std::endl;
         
         std::cout << "Example: TuneHell --verbose --redirection 7000;localhost;7001;+caesar(100)" <<
            ",+vigenere(bonjour),+html --redirection 7001;remote.host.eg;22;-html," <<
            "-vigenere(bonjour),-caesar(100)" << std::endl;
         return;
      }

      
      if (varMap.count("redirection")) {

         for (auto redirection : redirections) {
            TuneHell::CTunnel * newTunnel = parseRedirection(redirection);
            if (newTunnel != nullptr) {
               tunnels.emplace_back(newTunnel);
            }
         }
      }


      if (varMap.count("config-file")) {
         //Handle json config file loading
         // => https://github.com/nlohmann/json         
      }

   } catch (boost::program_options::required_option& e) {
      
      std::cerr << "Error: option " << e.get_option_name() << " should be specified." << std::endl 
         << std::endl;
      std::cerr << paramDesc << std::endl;
      return;

   } catch (boost::program_options::error& e) {

      std::cerr << "Error: " << e.what() << std::endl << std::endl;
      std::cerr << paramDesc << std::endl;
      return;

   }

}

int main(int argc, char *argv[])
{
   TuneHell::PythonHelper::InitPythonThreading();
   Py_BEGIN_ALLOW_THREADS

   std::list< std::unique_ptr<TuneHell::CTunnel> > tunnels;
   parseArgs(argc, argv, tunnels);

   std::list<std::thread> redirectionThreads;

   for (auto &tunnel : tunnels) {
      redirectionThreads.push_back(
         std::thread(
            std::bind(&TuneHell::CTunnel::Run, tunnel.release())
         )
      );
   }

   tunnels.clear();

   //Wait for threads to end
   std::for_each(redirectionThreads.begin(), redirectionThreads.end(),
      std::mem_fn(&std::thread::join));

   Py_END_ALLOW_THREADS
   TuneHell::PythonHelper::FinalizePythonThreading();

   return 0;
}
