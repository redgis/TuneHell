#include "../TuneHell.hpp"
#include "html.hpp"

namespace TuneHell
{
   
   std::size_t CHtml::C2STransform(CBuffer & buffer) {

      std::size_t readData = buffer.size();

      buffer.push_front(this->m_header + std::to_string(readData) + this->m_header2);
      buffer.push_back(this->m_footer);

      return readData;
   }

   std::size_t CHtml::C2SUnTransform(CBuffer & buffer) {
      
      std::size_t initialBufSize = buffer.size();

      tEncapsulationOffsets encapOffsets = { 0, 0, 0, 0, 0, 0 };
      tEncapsulationOffsets previousEncapOffsets = encapOffsets;

      std::size_t blockSize = 0;       //block size (i.e. including html tags)
      std::size_t totalDataSize = 0;   //payload size (i.e. excluding html tags)

      blockSize = this->getEncapsulationEndOffset(
         totalDataSize, buffer, encapOffsets);
         
      if (blockSize > 0) {
         //remove footer
         buffer.erase(totalDataSize + encapOffsets.footer_start,
            this->m_footer.length());

         //remove header
         buffer.erase(totalDataSize, encapOffsets.header2_end);

         previousEncapOffsets = encapOffsets;
         totalDataSize += encapOffsets.dataSize;

      } else { //blockSize == 0 , we didn't find a complete <HTML></HTML> block this time

         //remove remaining unread bytes (uncomplete <HTML></HTML> block)
         buffer.pop_back(buffer.size() - totalDataSize);
      }


      if (blockSize == 0) {
         buffer.clear();
      }

      return blockSize; // Used to indicate how many bytes of original buffer 
                        // have been treated.
   }

   /** Retrives data and info about the next HTML-encapsulated block.
    *  \param startOffset Starting position in the buffer, from which to look for a HTML 
    *    encapsulated block
    *  \param buffer data buffer in which to look for HTML encapsulation
    *  \param offsets structure tu be filled with informations on the positions of the various 
    *    elements of the encapsulation
    *  \return data size of the HTML encapsulated block, including the encapsulation
    */
   std::size_t CHtml::getEncapsulationEndOffset(
      std::size_t startOffset,
      CBuffer & buffer, 
      tEncapsulationOffsets & offsets)
   {
      assert(startOffset <= buffer.size());
      
      // Could have used std::regex, but is it really necessary for simple string searches ?

      const unsigned char * data = buffer.data() + startOffset;

      // Buffer contains at least the header ?
      if (buffer.size() <= this->m_header.length() + 6 + this->m_header2.length() + this->m_footer.length() ||
         strncmp((char*)(data),
            this->m_header.c_str(),
            this->m_header.length()))
         return 0;

      const unsigned char *dataSizeAddress = data + this->m_header.length();

      // Scan at most 6 digits before finding a '<'
      std::size_t digits = 0;
      for (digits = 0; digits < 6; digits++) {
         if (*(dataSizeAddress + digits) < 48 ||
            *(dataSizeAddress + digits) > 57)
         {
            if (*(dataSizeAddress + digits) == '<') {
               break;
            } else {
               return 0;
            }
         }
      }

      // Check presence of "...</TITLE></HEADER><BODY>"
      if (std::strncmp((char*)(dataSizeAddress + digits), this->m_header2.c_str(),
         this->m_header2.length()))
         return 0;

      // Read Data size
      std::size_t dataSize = std::atoi((char*)(dataSizeAddress));

      if (buffer.size() -
         (this->m_header.length() + digits + this->m_header2.length() + this->m_footer.length())
         < dataSize)
      {
         return 0;
      }

      // Find footer position
      const unsigned char * footerAddress = data + this->m_header.length() + digits +
         this->m_header2.length() + dataSize;

      if (std::strncmp((char*)footerAddress, this->m_footer.c_str(), this->m_footer.length())) {
         return 0;
      }

      // Fill output information
      std::size_t outputSize = (footerAddress + this->m_footer.length()) - data;

      offsets.header1_start = 0;
      offsets.header1_end = this->m_header.length();
      offsets.header2_start = this->m_header.length() + digits;
      offsets.header2_end = this->m_header.length() + digits + this->m_header2.length();
      offsets.footer_start = this->m_header.length() + digits + this->m_header2.length() + 
         dataSize;
      offsets.footer_end = this->m_header.length() + digits + this->m_header2.length() + dataSize +
         this->m_footer.length();
      offsets.dataSize = dataSize;

      return offsets.footer_end;
   }

   std::unique_ptr<CDataTransform> CHtml::clone() const {
      return std::unique_ptr<CDataTransform> (new CHtml(*this));
   }

} //TuneHell namespace