#pragma once

namespace TuneHell
{

   /** \copydoc TuneHell::CDataTransform
    *  HTTP protocol encapsulation.
    */
   class CHttp : public CDataTransform
   {
      private:
         void BuildHTTPRequest(CBuffer &buffer);
         void BuildHTTPResponse(CBuffer &buffer);

         std::size_t GetHTTPMessagePayload(CBuffer &buffer);

      public:
         CHttp();

         virtual std::size_t C2STransform(CBuffer &) override;
         virtual std::size_t C2SUnTransform(CBuffer &) override;
         virtual std::size_t S2CTransform(CBuffer &) override;
         virtual std::size_t S2CUnTransform(CBuffer &) override;

         virtual std::unique_ptr<CDataTransform> clone() const override;
   };

} //TuneHell namespace