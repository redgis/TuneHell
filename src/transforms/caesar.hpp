#pragma once

namespace TuneHell
{
   /** \copydoc TuneHell::CDataTransform
    *  Caesar cipher data transformation. https://en.wikipedia.org/wiki/Caesar_cipher
    */
   class CCaesar : public CDataTransform
   {
      private:

         const unsigned char m_key;    /**< Key used for caesar encryption / decryption */

      public:

         CCaesar(const unsigned char key);

         virtual std::size_t C2STransform(CBuffer &) override;
         virtual std::size_t C2SUnTransform(CBuffer &) override;

         virtual std::unique_ptr<CDataTransform> clone() const override;
   };

} //TuneHell namespace