#pragma once

namespace TuneHell
{
   /* Utility functions to make python threading work */
   namespace PythonHelper
   {
      void InitPythonThreading();
      void PreparePythonEnvironmentForThread();
      void FinalizePythonThreading();
   } //PythonHelper


   /** \copydoc TuneHell::CDataTransform
    *  Python data transformation that will call a python script.
    */
   class CPython : public CDataTransform
   {
      private:
         PyGILState_STATE gstate;      /**< Python Global Interpreter Lock (GIL) state */

         bool m_scriptHasBeenLoaded;   /**< True if Python scrript has been loaded already */
         std::string m_scriptFullPath; /**< Full path and filename of Python script */
         std::string m_scriptFilename; /**< Filename of Python script */
         std::string m_scriptPath;     /**< Path of Python script */
         std::string m_moduleName;     /**< Module name associated with Python script */

         PyObject * m_pyModule;              /**< Pointer to Python module for script */

         PyObject * m_pyC2STransformFunc;    /**< C2STransform function from Python script */
         PyObject * m_pyC2SUnTransformFunc;  /**< C2SUnTransform function from Python script */
         PyObject * m_pyS2CTransformFunc;    /**< S2CTransform function from Python script */
         PyObject * m_pyS2CUnTransformFunc;  /**< S2CUnTransform function from Python script */

         void LockPythonEnvironment();
         void ReleasePythonEnvironment();
         void LoadPythonScript();

         std::size_t CallPythonFunction(PyObject *pyFunction, CBuffer & buffer);

      public:
         CPython(std::string);
         virtual ~CPython();

         virtual std::size_t C2STransform(CBuffer &) override;
         virtual std::size_t C2SUnTransform(CBuffer &) override;
         virtual std::size_t S2CTransform(CBuffer &) override;
         virtual std::size_t S2CUnTransform(CBuffer &) override;

         virtual std::unique_ptr<CDataTransform> clone() const override;
   };

} //TuneHell namespace