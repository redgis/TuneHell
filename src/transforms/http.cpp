#include "../TuneHell.hpp"
#include "http.hpp"

namespace TuneHell
{
   CHttp::CHttp() {
      std::srand( static_cast<unsigned int>(std::time(NULL)) );
   }

   /** Constructs an HTTP request, encapsulating passed data (according to 
    *    https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)
    *  \param buffer buffer of data to be encapsulated in http request
    */
   void CHttp::BuildHTTPRequest(CBuffer &buffer) {
      std::string header;

      header += "POST /content/"
         + std::to_string(std::rand())
         + std::to_string(std::rand())
         + " HTTP/1.1\n";

      header += "Host: fbcdn-sphotos-a-a.akamaihd.net\r\n";
      header += "User - Agent: Mozilla / 5.0 (Windows NT 6.1; WOW64; rv:47.0;.NET CLR 4.0.30319;"
                "ffco7) Gecko / 20100101 Firefox / 47.0\r\n";
      header += "Accept: text / css, */*;q=0.1\r\n";
      header += "Accept-Language: en-US,en;q=0.8,fr-FR;q=0.5,fr;q=0.3\r\n";
      header += "Accept-Encoding: base64\r\n";
      header += "DNT: 1\r\n";
      header += "Connection: keep-alive\r\n";
      header += "Content-Length: " + std::to_string(buffer.size()) + "\r\n";
      header += "\r\n";
      
      buffer.push_front(header);
   }

   /** Constructs an HTTP response, encapsulating passed data (according to 
    *    https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)
    *  \param buffer buffer of data to be encapsulated in http response
    */
   void CHttp::BuildHTTPResponse(CBuffer &buffer) {
      std::string header;

      header += "HTTP/1.1 200 OK\r\n";
      std::time_t timeNow = std::time(NULL);

      header += "Date: ";
         header += std::asctime(std::gmtime(&timeNow));
         header[header.size() - 1] = ' ';
         header += " GMT\r\n";

      header += "Server: Apache\r\n";
      header += "Last-Modified: Sun, 22 Nov 2009 21:52:01 GMT\r\n";
      header += "Accept-Ranges: bytes\r\n";
      header += "Cache-Control: max-age=86400\r\n";
      header += "Vary: Accept-Encoding\r\n";
      header += "Content-Encoding: base64\r\n";
      header += "Content-Length: " + std::to_string(buffer.size()) + "\r\n";
      header += "Keep-Alive: timeout=5, max=98\r\n";
      header += "Connection: Keep-Alive\r\n";
      header += "Content-Type: text/css\r\n";
      header += "\r\n";

      buffer.push_front(header);
   }

   /** Extract data from the passed HTTP request ot response (according to 
    *    https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)
    *  \param buffer HTTP request or response from which to extract data
    *  \return the amount of data processed from the input buffer
    */
   std::size_t CHttp::GetHTTPMessagePayload(CBuffer &buffer) {
      const unsigned char lengthString[] = "Content-Length: ";

      //Find Content-Length
      const unsigned char *lengthPos =
         std::search(buffer.data(), buffer.data() + buffer.size(), lengthString, lengthString + 16);

      if (lengthPos >= buffer.data() + buffer.size())
         return 0;

      lengthPos += 16;

      std::size_t dataLength = std::atoi(reinterpret_cast<const char *>(lengthPos));

      if (dataLength == 0)
         return 0;

      //Find \r\n\r\n
      const unsigned char dataSeparation[] = "\r\n\r\n";

      const unsigned char *data =
         std::search(lengthPos, buffer.data() + buffer.size(), dataSeparation, dataSeparation + 4);

      if (data >= buffer.data() + buffer.size())
         return 0;

      data += 4; // data is just after "\r\n\r\n"

      //Calculate data length. Is it >= Content-Length ?
      std::size_t headerLength = data - buffer.data();

      if (buffer.size() - headerLength < dataLength)
         return 0;
      
      //Remove header and trailing data
      buffer.pop_front(headerLength);
      buffer.pop_back(buffer.size() - dataLength);

      return headerLength + dataLength;
   }

   std::size_t CHttp::C2STransform(CBuffer & buffer) {
      std::size_t result = buffer.size();

      BuildHTTPRequest(buffer);

      return result;
   }

   std::size_t CHttp::C2SUnTransform(CBuffer & buffer) {
      return GetHTTPMessagePayload(buffer);
   }

   std::size_t CHttp::S2CTransform(CBuffer & buffer) {
      std::size_t result = buffer.size();

      BuildHTTPResponse(buffer);

      return result;
   }

   std::size_t CHttp::S2CUnTransform(CBuffer & buffer) {
      return GetHTTPMessagePayload(buffer);
   }

   std::unique_ptr<CDataTransform> CHttp::clone() const {
      return std::unique_ptr<CDataTransform> (new CHttp(*this));
   }

} //TuneHell namespace