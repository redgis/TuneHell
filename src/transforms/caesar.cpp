#include "../TuneHell.hpp"
#include "caesar.hpp"

namespace TuneHell
{

   /** Constructs Caesar cipher data transformation.
    *  \param key Key used for caesar encryption / decryption
    */
   CCaesar::CCaesar(const unsigned char key)
      : m_key(key)
   {}

   /** \copydoc TuneHell::CDataTransform::C2STransform
    *  Transformation consists in the Caesar cipher encryption, based on the key provided at 
    *    construction.
    */
   std::size_t CCaesar::C2STransform(CBuffer & buffer) {

      for (std::size_t index = 0; index < buffer.size(); index++) {
         buffer[index] = (buffer[index] + this->m_key) % 256;
      }

      return buffer.size();
   }

   /** \copydoc TuneHell::CDataTransform::C2SUnTransform
    *  Transformation consists in the Caesar cipher decryption, based on the key provided at 
    *    construction.
    */
   std::size_t CCaesar::C2SUnTransform(CBuffer & buffer) {

      for (std::size_t index = 0; index < buffer.size(); index++) {
         buffer[index] = (buffer[index] - this->m_key) % 256;
      }

      return buffer.size();
   }

   std::unique_ptr<CDataTransform> CCaesar::clone() const {
      return std::unique_ptr<CDataTransform>(new CCaesar(*this));
   }

} //TuneHell namespace