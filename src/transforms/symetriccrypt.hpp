#pragma once

namespace TuneHell
{

   /** \copydoc TuneHell::CDataTransform
    *  Symetric key cryptographic transformation, using CryptoPP algorithm given as template 
    *  parameter.
    */
   template <class CryptoPPAlgo>
   class CSymetricCrypt: public CDataTransform
   {
      private:
         CryptoPP::SecByteBlock m_key;
         CryptoPP::SecByteBlock m_iv;
         typename CryptoPP::CFB_Mode<CryptoPPAlgo>::Encryption m_cfbEncryption;
         typename CryptoPP::CFB_Mode<CryptoPPAlgo>::Decryption m_cfbDecryption;

      public:
         CSymetricCrypt(std::string filename);

         virtual std::size_t C2STransform(CBuffer &) override;
         virtual std::size_t C2SUnTransform(CBuffer &) override;

         virtual std::unique_ptr<CDataTransform> clone() const override;
   };


   /** Construct cryptographic transformation, using given file content as key.
    *    \param filename Path to file containing the symetric key to use.
    */
   template <class CryptoPPAlgo>
   CSymetricCrypt<CryptoPPAlgo>::CSymetricCrypt(std::string filename) {

      //Waiting for VS2017 to support C++17 standard... (2018-11-20)
      //std::filesystem::path keypath(filename);
      //std::filesystem::directory_entry keyfile(keypath);
      //if (keyfile.is_file()) {
      //   size_t keySize = keyfile.file_size();
      //}

      byte *key = new byte[CryptoPPAlgo::MAX_KEYLENGTH];

      //CryptoPP::FileSource fs(filename.c_str(), true, 
      //new CryptoPP::ArraySink(key, CryptoPPAlgo::MAX_KEYLENGTH));

      std::ifstream fs(filename, std::ios::binary);
      size_t actualKeySize = std::min((size_t)fs.tellg(), (size_t)CryptoPPAlgo::MAX_KEYLENGTH);
      fs.read(reinterpret_cast<char *>(key), CryptoPPAlgo::MAX_KEYLENGTH);

      // Set key
      this->m_key = CryptoPP::SecByteBlock(key, actualKeySize);

      // Generate a random IV
      this->m_iv = CryptoPP::SecByteBlock(CryptoPPAlgo::BLOCKSIZE);
      CryptoPP::AutoSeededRandomPool rnd;
      rnd.GenerateBlock(this->m_iv, this->m_iv.size());

      // Set encryptor
      this->m_cfbEncryption = CryptoPP::CFB_Mode<CryptoPPAlgo>::Encryption(this->m_key,
         this->m_key.size(), this->m_iv);

      delete key;

   }

    template <class CryptoPPAlgo>
    std::size_t CSymetricCrypt<CryptoPPAlgo>::C2STransform(CBuffer & buffer) {

      // Encode buffer using crypto++
      std::size_t consumed = buffer.size();
      CBuffer::tYieldBuffer rawBuffer = buffer.detach();

      //https://www.cryptopp.com/wiki/Advanced_Encryption_Standard : The following example uses
      //CFB mode and in-place encryption and decryption. Since the mode is not ECB or CBC, the
      //data's length does not need to be a multiple of AES's block size.
      this->m_cfbEncryption.ProcessData(rawBuffer.buffer, rawBuffer.buffer, rawBuffer.size);

      buffer.attach(rawBuffer);

      return consumed;
   }

   template <class CryptoPPAlgo>
   std::size_t CSymetricCrypt<CryptoPPAlgo>::C2SUnTransform(CBuffer & buffer) {

      //Decode buffer using crypto++
      std::size_t consumed = buffer.size();
      CBuffer::tYieldBuffer rawBuffer = buffer.detach();

      //https://www.cryptopp.com/wiki/Advanced_Encryption_Standard : The following example uses
      //CFB mode and in-place encryption and decryption. Since the mode is not ECB or CBC, the
      //data's length does not need to be a multiple of AES's block size.
      this->m_cfbDecryption.ProcessData(rawBuffer.buffer, rawBuffer.buffer, rawBuffer.size);

      buffer.attach(rawBuffer);

      return consumed;
   }

   template <class CryptoPPAlgo>
   std::unique_ptr<CDataTransform> CSymetricCrypt<CryptoPPAlgo>::clone() const {
      return std::unique_ptr<CDataTransform>(new CSymetricCrypt(*this));
   }


} //TuneHell namespace