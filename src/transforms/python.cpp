#include "../TuneHell.hpp"
#include "python.hpp"

namespace TuneHell
{

   namespace PythonHelper
   {

      /** Helper function to initialize Python Interpreter. Should usually be called ONCE from main
       *    thread when program starts. */
      void InitPythonThreading() {
         if (!Py_IsInitialized()) {
            Py_Initialize();
         }
         PyEval_InitThreads();
      }

      /** Helper function to initialize Python Interpreter in any thread using it. */
      void PreparePythonEnvironmentForThread() {
         //nothing to do in this function for now.
      }

      /** Helper function to terminate Python Interpreter. Should usually be called ONCE from main
       *    thread when program ends. */
      void FinalizePythonThreading() {
         Py_Finalize();
      }

   }

   /** Build a transformation that will call the python script passed in parameter.
    *  /param script Python script full path and filename to use for data transformation. */
   CPython::CPython(std::string script)
      : m_scriptHasBeenLoaded(false), m_scriptFullPath(script), 
      m_pyC2STransformFunc(nullptr), m_pyC2SUnTransformFunc(nullptr),
      m_pyS2CTransformFunc(nullptr), m_pyS2CUnTransformFunc(nullptr)
   {
      if (verbose_mode) {
         std::cout << "Loading python script \"" + script + "\"..." << std::endl;
      }

      if (script.substr(script.length() - 3) != ".py") {
         if (verbose_mode) {
            std::cout << "Provided script \"" + script + 
               "\" doesn't seem to be python (name should end with \".py\") !" << std::endl;
         }
         return;
      }

      if (script.find_last_of("\\/") == std::string::npos) {
         this->m_scriptFilename = script;
         this->m_scriptPath = "";
      } else {
         this->m_scriptFilename = script.substr(script.find_last_of("\\/") + 1);
         this->m_scriptPath = script.substr(0, script.find_last_of("\\/") + 1);
      }

      this->m_moduleName = this->m_scriptFilename.substr(0, this->m_scriptFilename.length() - 3);
   }

   CPython::~CPython() {
      this->LockPythonEnvironment();
      Py_DECREF(this->m_pyModule);
      Py_DECREF(this->m_pyC2STransformFunc);
      Py_DECREF(this->m_pyC2SUnTransformFunc);
      Py_DECREF(this->m_pyS2CTransformFunc);
      Py_DECREF(this->m_pyS2CUnTransformFunc);
      this->ReleasePythonEnvironment();
   }

   /** Load the Python script passed in parameter at construction */
   void CPython::LoadPythonScript() {

      std::stringstream ss;
      ss << std::quoted(this->m_scriptPath, '\'');

      this->LockPythonEnvironment();

      PyRun_SimpleString("import sys");
      PyRun_SimpleString(("sys.path.append(" + ss.str() + ")").c_str());

      PyObject * pyModuleName = PyUnicode_FromString(this->m_moduleName.c_str());
      this->m_pyModule = PyImport_Import(pyModuleName);
      Py_DECREF(pyModuleName);

      if (this->m_pyModule == nullptr) {
         if (verbose_mode) {
            PyErr_Print();
            std::cout << "Error loading python script \"" + this->m_scriptFullPath +
               "\"." << std::endl;
            PyErr_Clear();
         }
         return;
      }

      this->m_pyC2STransformFunc = PyObject_GetAttrString(this->m_pyModule, (char*)"C2STransform");
      this->m_pyC2SUnTransformFunc = PyObject_GetAttrString(this->m_pyModule, (char*)"C2SUnTransform");

      if (verbose_mode) {
         if (this->m_pyC2STransformFunc == nullptr) {
            PyErr_Print();
            std::cout << "At least C2STransform and C2SUnTransform MUST be implemented in you Python script.";
         }

         if (this->m_pyC2SUnTransformFunc == nullptr) {
            PyErr_Print();
            std::cout << "At least C2STransform and C2SUnTransform MUST be implemented in you Python script.";
         }
         PyErr_Clear();
      }

      this->m_pyS2CTransformFunc = PyObject_GetAttrString(this->m_pyModule, (char*)"S2CTransform");
      this->m_pyS2CUnTransformFunc = PyObject_GetAttrString(this->m_pyModule, (char*)"S2CUnTransform");
      PyErr_Clear();

      this->m_scriptHasBeenLoaded = true;

      this->ReleasePythonEnvironment();
   }

   /** Lock Python Global Interpreter Lock (GIL) before using the interpreter. */
   void CPython::LockPythonEnvironment() {
      this->gstate = PyGILState_Ensure();
   }

   /** Release Python Global Interpreter Lock (GIL) after using the interpreter. */
   void CPython::ReleasePythonEnvironment() {
      PyGILState_Release(this->gstate);
   }


   /** Calls the given Python function, passing it the given buffer data as bytes array. This is 
    *    used to call the 4 Python functions that can be implemented in the Python script : 
    *    C2STransform, C2SUnTransform, S2CTransform, S2CUnTransform.
    *  /param pyFunction Python function to ba called.
    *  /param buffer data passed to the python function 
    *  /return the number of bytes consumed by the Python function */
   std::size_t CPython::CallPythonFunction(PyObject *pyFunction, CBuffer & buffer) {
      if (pyFunction == nullptr)
         return 0;

      CBuffer::tYieldBuffer rawBuffer = buffer.detach();
      
      this->LockPythonEnvironment();

      //Prepare argument list for method call
      PyObject *arg1 = Py_BuildValue("y#",
         reinterpret_cast<char*>(rawBuffer.buffer + rawBuffer.offset),
         rawBuffer.size
      );
      PyObject *argList = PyTuple_Pack(1, arg1);

      //Call python method
      PyObject *result = PyObject_CallObject(pyFunction, argList);

      std::size_t consumed = 0;

      CBuffer::tYieldBuffer tempBuffer = { 0, 0, 0, 0 };

      // if no result has been returned, there was probably an error. Report it.
      if (result == nullptr) {
         if (verbose_mode) {
            PyErr_Print();
            PyErr_Clear();
         }
      } else {

         //The python function is supposed to return list of two elements : the transformed data
         //as a bytes array first, the number of bytes consumed from the input data buffer.
         PyObject *resultBuffer = PyList_GetItem(result, 0);
         PyObject *resultConsumedBytes = PyList_GetItem(result, 1);

         //Retrieve resulting data buffer
         PyBytes_AsStringAndSize(resultBuffer,
            reinterpret_cast<char **>(&tempBuffer.buffer),
            reinterpret_cast<Py_ssize_t *>(&tempBuffer.size));

         //Retrieve consumed bytes count
         PyErr_Clear();
         /*consumed = PyFloat_AsDouble(resultConsumedBytes);*/
         consumed = PyLong_AsUnsignedLongLong(resultConsumedBytes);
         
         Py_DECREF(resultBuffer);
         Py_DECREF(resultConsumedBytes);

         if (verbose_mode) {
            PyErr_Print();
         }
      }
      
      Py_DECREF(result);
      Py_DECREF(arg1);
      Py_DECREF(argList);
      

      buffer.attach(rawBuffer);
      buffer.clear();
      buffer.push_back(tempBuffer.buffer + tempBuffer.offset, tempBuffer.size);

      this->ReleasePythonEnvironment();

      return consumed;
   }


   std::size_t CPython::C2STransform(CBuffer & buffer) {
      if (!this->m_scriptHasBeenLoaded)
         this->LoadPythonScript();

      return this->CallPythonFunction(this->m_pyC2STransformFunc, buffer);
   }

   std::size_t CPython::C2SUnTransform(CBuffer & buffer) {
      if (!this->m_scriptHasBeenLoaded)
         this->LoadPythonScript();

      return this->CallPythonFunction(this->m_pyC2SUnTransformFunc, buffer);
   }

   std::size_t CPython::S2CTransform(CBuffer & buffer) {
      if (!this->m_scriptHasBeenLoaded)
         this->LoadPythonScript();

      if (this->m_pyS2CTransformFunc == nullptr) {
         return this->CDataTransform::S2CTransform(buffer);
      } else {
         return this->CallPythonFunction(this->m_pyS2CTransformFunc, buffer);
      }
   }

   std::size_t CPython::S2CUnTransform(CBuffer & buffer) {
      if (!this->m_scriptHasBeenLoaded)
         this->LoadPythonScript();

      if (this->m_pyS2CUnTransformFunc == nullptr) {
         return this->CDataTransform::S2CUnTransform(buffer);
      } else {
         return this->CallPythonFunction(this->m_pyS2CUnTransformFunc, buffer);
      }
   }

   std::unique_ptr<CDataTransform> CPython::clone() const {
      return std::unique_ptr<CDataTransform>(new CPython(*this));
   }

} //TuneHell namespace