#pragma once

namespace TuneHell
{

   /** \copydoc TuneHell::CDataTransform
    *  Vigenere cipher data transformation. https://en.wikipedia.org/wiki/Vigen%C3%A8re_cipher
    */
   class CVigenere: public CDataTransform
   {
      private:

         const unsigned char *m_key;      /**< Key used for vigenere encryption / decryption */
         const std::size_t m_keylength;   /**< Key length used for vigenere encryption / decryption */

         //The encryption / decryption key is several bytes long. Since we are applying encryption
         //and decription to a data stream, we need to keep an index on where the last 
         //transformation stopped in the key, in order to continue from the same index for the 
         //upcoming data transformations.
         std::size_t m_C2STransformKeyIndex;
         std::size_t m_C2SUnTransformKeyIndex;
         std::size_t m_S2CTransformKeyIndex;
         std::size_t m_S2CUnTransformKeyIndex;

      public:

         CVigenere(const unsigned char * key, std::size_t keylen);

         virtual std::size_t C2STransform(CBuffer &) override;
         virtual std::size_t C2SUnTransform(CBuffer &) override;

         virtual std::size_t S2CTransform(CBuffer &) override;
         virtual std::size_t S2CUnTransform(CBuffer &) override;

         virtual std::unique_ptr<CDataTransform> clone() const override;
   };

} //TuneHell namespace