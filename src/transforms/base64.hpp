#pragma once

namespace TuneHell
{

   /** \copydoc TuneHell::CDataTransform
    *  base64 data transformation.
    */
   class CBase64 : public CDataTransform
   {
      private:

      public:
         virtual std::size_t C2STransform(CBuffer &) override;
         virtual std::size_t C2SUnTransform(CBuffer &) override;

         virtual std::unique_ptr<CDataTransform> clone() const override;
   };

} //TuneHell namespace