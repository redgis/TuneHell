#include "../TuneHell.hpp"
#include "vigenere.hpp"

namespace TuneHell
{

   /** Constructs Caesar cipher data transformation.
    *  \param key Key used for vigenere encryption / decryption
    *  \param keylen Length of the key used for vigenere encryption / decryption
    */
   CVigenere::CVigenere(const unsigned char *key, std::size_t keylen)
      :  m_key(new unsigned char[keylen]), m_keylength(keylen), 
         m_C2STransformKeyIndex(0), m_C2SUnTransformKeyIndex(0), 
         m_S2CTransformKeyIndex(0), m_S2CUnTransformKeyIndex(0)
   {
      std::memcpy((void *)this->m_key, key, keylen);
   }

   /** \copydoc TuneHell::CDataTransform::C2STransform
    *  Transformation consists in the vigenere cipher encryption, based on the key provided at 
    *    construction.
    */
   std::size_t CVigenere::C2STransform(CBuffer & buffer) {

      for (std::size_t index = 0; index < buffer.size(); index++) {
         buffer[index] = (buffer[index] + this->m_key[this->m_C2STransformKeyIndex]) % 256;

         this->m_C2STransformKeyIndex++;
         this->m_C2STransformKeyIndex %= this->m_keylength;
      }

      return buffer.size();
   }

   /** \copydoc TuneHell::CDataTransform::C2SUnTransform
    *  Transformation consists in the vigenere cipher decryption, based on the key provided at 
    *    construction.
    */
   std::size_t CVigenere::C2SUnTransform(CBuffer & buffer) {

      for (std::size_t index = 0; index < buffer.size(); index++) {
         buffer[index] = (buffer[index] - this->m_key[this->m_C2SUnTransformKeyIndex]) % 256;

         this->m_C2SUnTransformKeyIndex++;
         this->m_C2SUnTransformKeyIndex %= this->m_keylength;
      }

      return buffer.size();
   }

   /** \copydoc TuneHell::CDataTransform::S2CTransform
    *  Transformation consists in the vigenere cipher encryption, based on the key provided at 
    *    construction.
    */
   std::size_t CVigenere::S2CTransform(CBuffer & buffer) {

      for (std::size_t index = 0; index < buffer.size(); index++) {
         buffer[index] = (buffer[index] + this->m_key[this->m_S2CTransformKeyIndex]) % 256;

         this->m_S2CTransformKeyIndex++;
         this->m_S2CTransformKeyIndex %= this->m_keylength;
      }

      return buffer.size();
   }

   /** \copydoc TuneHell::CDataTransform::S2CUnTransform
    *  Transformation consists in the vigenere cipher decryption, based on the key provided at 
    *    construction.
    */
   std::size_t CVigenere::S2CUnTransform(CBuffer & buffer) {

      for (std::size_t index = 0; index < buffer.size(); index++) {
         buffer[index] = (buffer[index] - this->m_key[this->m_S2CUnTransformKeyIndex]) % 256;

         this->m_S2CUnTransformKeyIndex++;
         this->m_S2CUnTransformKeyIndex %= this->m_keylength;
      }

      return buffer.size();
   }

   std::unique_ptr<CDataTransform> CVigenere::clone() const {
      return std::unique_ptr<CDataTransform> (new CVigenere(*this));
   }

} //TuneHell namespace