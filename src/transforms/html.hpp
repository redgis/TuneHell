#pragma once

namespace TuneHell
{

   /** \copydoc TuneHell::CDataTransform
    *  HTML encapsulation data transformation. Simply adds 
    * "<HTML><HEADER><TITLE>[buffer size]</TITLE></HEADER><BODY>" in front of the data and 
    * "</BODY></HTML>" at the end.
    */
   class CHtml : public CDataTransform
   {
      private:
         typedef struct sEncapsulationOffsets {
            std::size_t header1_start; // ^<HTML>...
            std::size_t header1_end;   // ...<TITLE>^
            std::size_t header2_start; // ^</TITLE>...
            std::size_t header2_end;   // ...<BODY>^
            std::size_t footer_start; // ^</BODY>...
            std::size_t footer_end;   // ...</HTML>^
            std::size_t dataSize;
         } tEncapsulationOffsets;

         const std::string m_header = "<HTML><HEADER><TITLE>";    /**< HTML encapsulation header part 1*/
         const std::string m_header2 = "</TITLE></HEADER><BODY>"; /**< HTML encapsulation header part 2*/
         const std::string m_footer = "</BODY></HTML>";           /**< HTML encapsulation footer */

         std::size_t getEncapsulationEndOffset(
            std::size_t startOffset,
            CBuffer & buffer,
            tEncapsulationOffsets & offsets);
      public:

         virtual std::size_t C2STransform(CBuffer &) override;
         virtual std::size_t C2SUnTransform(CBuffer &) override;

         virtual std::unique_ptr<CDataTransform> clone() const override;
   };

} //TuneHell namespace