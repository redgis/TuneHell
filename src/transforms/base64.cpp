#include "../TuneHell.hpp"
#include "base64.hpp"

namespace TuneHell
{
   
   std::size_t CBase64::C2STransform(CBuffer & buffer) {
      
      // Encode buffer using crypto++
      std::size_t consumed = buffer.size();
      CBuffer::tYieldBuffer rawBuffer = buffer.detach();

      CryptoPP::Base64Encoder base64Encoder(nullptr, false, 80);
      consumed -= base64Encoder.Put(rawBuffer.buffer + rawBuffer.offset, rawBuffer.size, true);
      base64Encoder.MessageEnd();
      
      buffer.reserve(base64Encoder.MaxRetrievable() + 10);
      CBuffer::tYieldBuffer resultBuffer = buffer.detach();
      resultBuffer.offset = 10;
      resultBuffer.size = base64Encoder.Get(resultBuffer.buffer + resultBuffer.offset, 
         resultBuffer.capacity);
      buffer.attach(resultBuffer);

      // Add a "hidden" header providing the size of the encoded buffer. Necessary for decoding.
      std::string bufferLength = std::to_string(buffer.size()) + 'f';
      buffer.push_front(bufferLength);

      delete rawBuffer.buffer;

      return consumed;
   }

   std::size_t CBase64::C2SUnTransform(CBuffer & buffer) {

      std::size_t consumed = 0;

      // Retrieve encoded data size
      const unsigned char *realBufferStart = std::find(buffer.data(), 
         buffer.data() + buffer.size(), 'f') + 1;

      if (realBufferStart >= buffer.data() + buffer.size())
         return 0;

      std::size_t bufferLength = std::atoi( 
         reinterpret_cast<const char *>(buffer.data()) );

      if (bufferLength <= 0)
         return 0;

      // Remove buffer size and 'f' from buffer
      consumed = realBufferStart - buffer.data();
      buffer.pop_front(realBufferStart - buffer.data());

      if (buffer.size() < bufferLength)
         return 0;

      // Decode buffer using crypto++
      CBuffer::tYieldBuffer rawBuffer = buffer.detach();

      CryptoPP::Base64Decoder base64Decoder(nullptr);
      consumed += bufferLength;
      consumed -= base64Decoder.Put(rawBuffer.buffer + rawBuffer.offset, bufferLength, true);
      base64Decoder.MessageEnd();
      
      buffer.reserve(base64Decoder.MaxRetrievable());
      CBuffer::tYieldBuffer resultBuffer = buffer.detach();
      resultBuffer.offset = 0;
      resultBuffer.size = base64Decoder.Get(resultBuffer.buffer, resultBuffer.capacity);
      buffer.attach(resultBuffer);

      delete rawBuffer.buffer;

      return consumed;
   }

   std::unique_ptr<CDataTransform> CBase64::clone() const {
      return std::unique_ptr<CDataTransform>(new CBase64(*this));
   }

} //TuneHell namespace