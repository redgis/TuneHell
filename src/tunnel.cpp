#include "TuneHell.hpp"
#include "tunnel.hpp"

namespace TuneHell 
{

   /** Constructs a tunnel definition
    *  \param localPort local listening port
    *  \param targetHost remote host to which the tunnel forwards data
    *  \param targetPort remote port to which the tunnel forwards data
    */
   CTunnel::CTunnel(tPort localPort, std::string targetHost, tPort targetPort)
         :
      m_ioService(),
      m_localPort(localPort),
      m_remoteHost(targetHost),
      m_remotePort(targetPort),
      m_listening_endpoint(tcp::v4(), localPort),
      m_resolver(this->m_ioService),
      m_target_resolution_query(tcp::v4(), targetHost, std::to_string(targetPort)),
      m_listener(this->m_ioService, this->m_listening_endpoint)
   {
   }
   
   /** Starts listening to incoming connection.
    */
   void CTunnel::StartListening() {
      this->m_listener.listen();

      this->m_newTunnel = std::unique_ptr<CEstablishedTunnel>(
         new CEstablishedTunnel(*this, this->m_DataTransforms));

      this->m_listener.async_accept(this->m_newTunnel->getInputSocket(),
         boost::bind(&TuneHell::CTunnel::OnAccept,
            this, boost::asio::placeholders::error));
   }

   /** Add a data transformation to be given to all established tunnels based on this tunnel, 
    *    so that it will be applied to incoming data before forwarding it to remote host.
    *  \param transformation transformation to be added
    */
   void CTunnel::addTransformation(std::unique_ptr<TuneHell::CDataTransform> transformation) {
      this->m_DataTransforms.push_back(std::move(transformation));
   }

   /** Get current list of transformations defined in this tunnel.
    *  \return the list of transformations
    */
   std::list< std::unique_ptr<CDataTransform> > & CTunnel::getTransformations() {
      return this->m_DataTransforms;
   }

   /** Callback function triggered when an incoming connection is accepted.
    *  \param accept_ec error code, if any, associated with the connection
    */
   void CTunnel::OnAccept(const boost::system::error_code &accept_ec) {

      if (accept_ec) {

         std::cerr << "Error accepting connection on \"" << this->m_listening_endpoint.address()
            << ":" << this->m_listening_endpoint.port() << "\"" << std::endl;
         std::cerr << "   " << accept_ec.message() << std::endl;

      } else {
         
         //Print debug output
         if (verbose_mode) {
            boost::system::error_code ec;

            auto remote_ep = this->m_newTunnel->getInputSocket().remote_endpoint(ec);
            auto local_ep = this->m_newTunnel->getInputSocket().local_endpoint(ec);
            std::cout << "Accepted connection from \"" 
               << remote_ep.address() << ":" << remote_ep.port() 
               << "\" on \"" 
               << local_ep.address() << ":" << local_ep.port() << "\"" << std::endl;
         }

         //Establish outgoing connexion
         tcp::resolver::iterator target_endpoint_iterator =
            this->m_resolver.resolve(this->m_target_resolution_query);

         boost::system::error_code connect_ec;

         asio::connect(this->m_newTunnel->getOutputSocket(),
            target_endpoint_iterator, connect_ec);

         if (connect_ec) {
            this->m_newTunnel->getOutputSocket().close();

            std::cerr << "Error connecting to \"" << this->m_target_resolution_query.host_name()
               << ":" << this->m_target_resolution_query.service_name() << "\"" << std::endl;
            std::cerr << "   " << connect_ec.message() << std::endl;
         } else {

            if (verbose_mode) {
               boost::system::error_code ec;

               auto remote_ep = this->m_newTunnel->getOutputSocket().remote_endpoint(ec);
               auto local_ep = this->m_newTunnel->getOutputSocket().local_endpoint(ec);
               std::cout << "Established connection to \""
                  << remote_ep.address() << ":" << remote_ep.port()
                  << "\" from \""
                  << local_ep.address() << ":" << local_ep.port() << "\"" << std::endl;
            }

            //Start reading it.
            this->m_newTunnel->StartReading();

            //Add connection to list of established connections
            this->m_establishedTunnels.push_back(std::move(this->m_newTunnel));
            
            // Re-setup the temp connexion waiting for acception
            this->m_newTunnel = std::unique_ptr<CEstablishedTunnel>(
               new CEstablishedTunnel(*this, this->m_DataTransforms));
         }

         //Re-setup listening to incoming connections process
         this->m_listener.async_accept(this->m_newTunnel->getInputSocket(),
            boost::bind(&TuneHell::CTunnel::OnAccept,
               this, boost::asio::placeholders::error));
      }
   }
   
   /** Retrieve the Boost::Asio::io_service object, used to manipulate the data stream once the
    *    tunnel is established
    *  \return Boost::Asio::io_service object
    */
   asio::io_service &CTunnel::getIoService() {
      return this->m_ioService;
   }

   /** Retrieve the tunnel local TCP port on which it listens
    *  \return the local TCP port
    */
   tPort CTunnel::getLocalPort() {
      return this->m_localPort;
   }

   /** Retrieve the tunnel remote TCP port on which it connects
    *  \return the remote TCP port
    */
   tPort CTunnel::getRemotePort() {
      return this->m_remotePort;
   }

   /** Retrieve the tunnel remote host on which it connects
    *  \return the remote host
    */
   std::string CTunnel::getRemoteHost() {
      return this->m_remoteHost;
   }

   /** Close finds the givin established tunnel, closes and destroyes it.
    *  \param establishedTunnel the established tunnel to be looked up in active established
    *    tunnels associated to this tunnel, and closed.
    */
   void CTunnel::CloseEstablishedTunnel(CEstablishedTunnel * establishedTunnel) {

      if (verbose_mode) {

         boost::system::error_code ec;

         auto remote_ep = establishedTunnel->getInputSocket().remote_endpoint(ec);
         auto local_ep = establishedTunnel->getInputSocket().local_endpoint(ec);
         std::cout << "   Closed tunnel from \""
            << remote_ep.address() << ":" << remote_ep.port()
            << "\" on \""
            << local_ep.address() << ":" << local_ep.port() << "\"" << std::endl;

         remote_ep = establishedTunnel->getOutputSocket().remote_endpoint(ec);
         local_ep = establishedTunnel->getOutputSocket().local_endpoint(ec);
         std::cout << "                 to \""
            << remote_ep.address() << ":" << remote_ep.port()
            << "\" from \""
            << local_ep.address() << ":" << local_ep.port() << "\"" << std::endl;

      }

      this->m_establishedTunnels.remove_if(
         [&establishedTunnel](auto &tunnel) {
            return tunnel.get() == establishedTunnel; 
         }
      );
   }

   /** Runs the tunnel, as a boost::asio task. Starts listening to incoming connections and creates
    *    the corresponding redirection when this happens.
    */
   void CTunnel::Run() {
      TuneHell::PythonHelper::PreparePythonEnvironmentForThread();
      this->StartListening();
      this->m_ioService.run();
   }

} //TuneHell namespace