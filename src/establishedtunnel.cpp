#include "TuneHell.hpp"
#include "establishedtunnel.hpp"

namespace TuneHell
{
   /** Constructs a newly established tunnel.
    *  \param tunnel redirection definition on which this established tunnel is based
    *  \param transforms list of transformations this established tunnel must apply to passing 
    *    data. This list is used as a model and is copied inside the established tunnel.
    */
   CEstablishedTunnel::CEstablishedTunnel(CTunnel &tunnel, 
      std::list< std::unique_ptr<TuneHell::CDataTransform> > &transforms)
      :
      m_tunnel(tunnel),
      m_inputSocket(tunnel.getIoService()),
      m_outputSocket(tunnel.getIoService()),
      m_terminating(false)
   {
      for (auto &transform : transforms) {
         this->m_transforms.emplace_back( transform->clone() );
      }
   }

   /** Destroys this established tunnel and associated data buffers.
    */
   CEstablishedTunnel::~CEstablishedTunnel() {
      if (this->m_c2sReadBuffer.buffer != nullptr) {
         delete this->m_c2sReadBuffer.buffer;
      }

      if (this->m_s2cReadBuffer.buffer != nullptr) {
         delete this->m_s2cReadBuffer.buffer;
      }
   };

   /** Retrieve socket holding the connection with the client.
    *  \return the input socket
    */
   tcp::socket & CEstablishedTunnel::getInputSocket() {
      return this->m_inputSocket;
   }

   /** Retrieve socket holding the connection with the server.
    *  \return the output socket
    */
   tcp::socket & CEstablishedTunnel::getOutputSocket() {
      return this->m_outputSocket;
   }

   /** Retrieve TuneHell::CTunnel object, redirection definition on which this established tunnel 
    *    is based.
    *  \return the redirection definition
    */
   CTunnel &CEstablishedTunnel::getTunnel() {
      return this->m_tunnel;
   }

   /** Posts a boost::asio task to read data coming from client socket.
    *  \param buffer a loose buffer in which recieved data will be stored.
    */
   void CEstablishedTunnel::PostC2SReadingRequest(CBuffer::tYieldBuffer & buffer) {
      this->m_inputSocket.async_read_some(
         asio::buffer(
            buffer.buffer + buffer.offset + buffer.size,
            buffer.capacity - (buffer.offset + buffer.size)),
         boost::bind(
            &TuneHell::CEstablishedTunnel::OnReadFromInput,
            this,
            asio::placeholders::error, asio::placeholders::bytes_transferred)
         );
   }

   /** Posts a boost::asio task to read data coming from server socket.
    *  \param buffer a loose buffer in which recieved data will be stored.
    */
   void CEstablishedTunnel::PostS2CReadingRequest(CBuffer::tYieldBuffer & buffer) {
      this->m_outputSocket.async_read_some(
         asio::buffer(
            buffer.buffer + buffer.offset + buffer.size,
            buffer.capacity - (buffer.offset + buffer.size)),
         boost::bind(
            &TuneHell::CEstablishedTunnel::OnReadFromTarget,
            this,
            asio::placeholders::error, asio::placeholders::bytes_transferred)
         );
   }

   /** Sets up everything so that the tunnel can start listening to incoming data from client and
    *    from server.
    */
   void CEstablishedTunnel::StartReading() {

      if (log_mode) {
         std::string C2Sfilename = 
            this->getInputSocket().remote_endpoint().address().to_string() + "_" +
            std::to_string(this->m_tunnel.getLocalPort()) + "_to_" +
            this->m_tunnel.getRemoteHost() + "_" +
            std::to_string(this->m_tunnel.getRemotePort()) + "_C2S.dump";

         std::string S2Cfilename = 
            this->m_tunnel.getRemoteHost() + "_" +
            std::to_string(this->m_tunnel.getRemotePort()) +  "_to_" +
            this->getInputSocket().remote_endpoint().address().to_string() + "_" +
            std::to_string(this->m_tunnel.getLocalPort()) + "_S2C.dump";

         this->C2Sdump.open(C2Sfilename, std::ios::out | std::ios::trunc | std::ios::binary);
         this->S2Cdump.open(S2Cfilename, std::ios::out | std::ios::trunc | std::ios::binary);
      }
      
      //Prepare buffer for client incoming data, and post boost::asio reading task
      this->m_c2sReadBuffer.capacity = 5120;
      this->m_c2sReadBuffer.buffer = new unsigned char[this->m_c2sReadBuffer.capacity];
      this->m_c2sReadBuffer.offset = 0;
      this->m_c2sReadBuffer.size = 0;

      this->PostC2SReadingRequest(this->m_c2sReadBuffer);

      //Prepare buffer for server incoming data, and post boost::asio reading task
      this->m_s2cReadBuffer.capacity = 5120;
      this->m_s2cReadBuffer.buffer = new unsigned char[this->m_s2cReadBuffer.capacity];
      this->m_s2cReadBuffer.offset = 0;
      this->m_s2cReadBuffer.size = 0;

      this->PostS2CReadingRequest(this->m_s2cReadBuffer);
   }

   /** Transforms as much data possible, recieved from client, destined to server, from an input 
    *    buffer, into an output buffer.
    *  \param inputBuffer input buffer from which to transform data
    *  \param outputBuffer output buffer into which to transform data
    *  \return amount of data from inputBuffer that was transformed.
    */
   std::size_t CEstablishedTunnel::processC2STransform(CBuffer &inputBuffer, 
      CDataTransform &transform, CBuffer &outputBuffer)
   {
      if (inputBuffer.size() == 0)
         return 0;

      std::size_t readBytes = 0;
      std::size_t totalReadBytes = 0;

      CBuffer workingBuffer;

      do {
         
         workingBuffer.copy(inputBuffer);

         if (!transform.isReversed()) {
            readBytes = transform.C2STransform(workingBuffer);
         } else {
            readBytes = transform.C2SUnTransform(workingBuffer);
         }

         totalReadBytes += readBytes;

         if (readBytes > 0) {
            outputBuffer.push_back(workingBuffer);
            inputBuffer.pop_front(readBytes);
         }

      } while (readBytes > 0);

      return totalReadBytes;
   }

   /** Transforms as much data possible, recieved from server, destined to client, from an input 
    *    buffer, into an output buffer.
    *  \param inputBuffer input buffer from which to transform data
    *  \param outputBuffer output buffer into which to transform data
    *  \return amount of data from inputBuffer that was transformed.
    */
   std::size_t CEstablishedTunnel::processS2CTransform(CBuffer &inputBuffer,
      CDataTransform &transform, CBuffer &outputBuffer) 
   {
      if (inputBuffer.size() == 0)
         return 0;

      std::size_t readBytes = 0;
      std::size_t totalReadBytes = 0;

      CBuffer workingBuffer;

      do {

         workingBuffer.copy(inputBuffer);

         if (!transform.isReversed()) {
            readBytes = transform.S2CUnTransform(workingBuffer);
         } else {
            readBytes = transform.S2CTransform(workingBuffer);
         }

         totalReadBytes += readBytes;

         if (readBytes > 0) {
            outputBuffer.push_back(workingBuffer);
            inputBuffer.pop_front(readBytes);
         }

      } while (readBytes > 0);

      return totalReadBytes;
   }


   /** This method is a callback triggered when boost::asio revieved data from client.
    *  \param ec error code provided by boost::asio
    *  \param bytes_transferred size of recieved data
    */
   void CEstablishedTunnel::OnReadFromInput(
      const boost::system::error_code &ec,
      std::size_t bytes_transferred)
   {
      if (this->m_terminating) {
         return;
      }

      this->m_c2sReadBuffer.size += bytes_transferred;

      if ((!ec || ec == asio::error::eof) && bytes_transferred > 0) 
      {
         if (log_mode) {
            this->C2Sdump << std::endl << std::endl << "--> " << std::time(nullptr) << " " <<
               this->m_c2sReadBuffer.size << " " << std::flush;
            boost::crc_32_type inputCRC;
            inputCRC.process_bytes(this->m_c2sReadBuffer.buffer + this->m_c2sReadBuffer.offset, 
               this->m_c2sReadBuffer.size);
            this->C2Sdump << inputCRC.checksum() << std::endl << std::flush;
            this->C2Sdump.write(
               reinterpret_cast<const char*>(
                  this->m_c2sReadBuffer.buffer + this->m_c2sReadBuffer.offset),
               this->m_c2sReadBuffer.size);
            this->C2Sdump << std::endl << std::flush;
         }
         
         // Apply transformations
         std::size_t readBytes = 0;
         std::size_t totalReadBytes = 0;

         auto itFirstTransform = this->m_transforms.begin();
         auto itLastTransform = this->m_transforms.end();
         auto itCurTransform = itFirstTransform;
         
         CBuffer inputBuffer;
         inputBuffer.copy(this->m_c2sReadBuffer);

         CBuffer outputBuffer;

         while (itCurTransform != itLastTransform) {
            outputBuffer.clear();

            readBytes = this->processC2STransform(inputBuffer, *(*itCurTransform), 
               outputBuffer);

            inputBuffer.copy(outputBuffer);

            if (itCurTransform == itFirstTransform) {
               totalReadBytes = readBytes;
            }

            itCurTransform++;
         }

         if (outputBuffer.size() > 0) {
            //Discard the data read from the main buffer
            CBuffer tmpBuf = CBuffer(this->m_c2sReadBuffer);
            tmpBuf.pop_front(totalReadBytes);
            this->m_c2sReadBuffer = tmpBuf.detach();

            //Send result of transformations to remote host
            this->WriteToTarget(outputBuffer);
            
            if (log_mode) {
               this->C2Sdump << "<-- " << std::time(nullptr) << " " << totalReadBytes << " " <<
                  outputBuffer.size() << " " << std::flush;
               boost::crc_32_type outputCRC;
               outputCRC.process_bytes(outputBuffer.data(), outputBuffer.size());
               this->C2Sdump << outputCRC.checksum() << std::endl << std::flush;
               this->C2Sdump.write(
                  reinterpret_cast<const char*>(outputBuffer.data()),
                  outputBuffer.size());
               this->C2Sdump << std::flush;
            }
         }
      }

      // If no error occured, re-setup the read handler
      if (!ec && !this->m_terminating) {
         CBuffer tmpBuf = CBuffer(this->m_c2sReadBuffer);

         // Need to reallocate ?
         if (tmpBuf.rightSpaceRemaining() < 1024) {

            if (tmpBuf.leftSpaceRemaining() < 1024) {
               tmpBuf.reserve(tmpBuf.capacity() + 1024);
            }

            tmpBuf.resetOffset(0);
         }

         this->m_c2sReadBuffer = tmpBuf.detach();
         this->PostC2SReadingRequest(this->m_c2sReadBuffer);

         // If eof (socket closed), don't re-setup the input handler
      } else if (ec == asio::error::eof) {
         
         if (!this->m_terminating) {
            this->m_terminating = true;

            CTunnel &tunnel = this->m_tunnel;
            tunnel.getIoService().post(
               [&] {
                  this->getTunnel().CloseEstablishedTunnel(this);
               }
            );
         }
      } else {
         // Remove this from establised connections list
         if (!this->m_terminating) {
            this->m_terminating = true;

            CTunnel &tunnel = this->m_tunnel;
            tunnel.getIoService().post(
               [&] {
                  this->getTunnel().CloseEstablishedTunnel(this);
               }
            );
         }
      }

   }

   /** Sends data to server.
    *  \param buf data to be sent
    */
   void CEstablishedTunnel::WriteToTarget(CBuffer & buf) {
      boost::system::error_code ec;

      std::size_t dataWritten = asio::write(
         this->m_outputSocket,
         asio::buffer(buf.data(), buf.size()),
         asio::transfer_all(),
         ec);
   }

   /** This method is a callback triggered when boost::asio revieved data from server.
    *  \param ec error code provided by boost::asio
    *  \param bytes_transferred size of recieved data
    */
   void CEstablishedTunnel::OnReadFromTarget(
      const boost::system::error_code &ec,
      std::size_t bytes_transferred)
   {
      if (this->m_terminating) {
         return;
      }

      this->m_s2cReadBuffer.size += bytes_transferred;

      if ((!ec || ec == asio::error::eof) && bytes_transferred > 0)
      {
         if (log_mode) {
            this->S2Cdump << std::endl << std::endl << "--> " << std::time(nullptr) << " " <<
               this->m_s2cReadBuffer.size << " " << std::flush;
            boost::crc_32_type inputCRC;
            inputCRC.process_bytes(this->m_s2cReadBuffer.buffer + this->m_s2cReadBuffer.offset,
               this->m_s2cReadBuffer.size);
            this->S2Cdump << inputCRC.checksum() << std::endl << std::flush;
            this->S2Cdump.write(
               reinterpret_cast<const char*>(
                  this->m_s2cReadBuffer.buffer + this->m_s2cReadBuffer.offset),
               this->m_s2cReadBuffer.size);
            this->S2Cdump << std::endl << std::flush;
         }

         // Apply transformations
         std::size_t readBytes = 0;
         std::size_t totalReadBytes = 0;

         auto itFirstTransform = this->m_transforms.rbegin();
         auto itLastTransform = this->m_transforms.rend();
         auto itCurTransform = itFirstTransform;

         CBuffer inputBuffer;
         inputBuffer.copy(this->m_s2cReadBuffer);

         CBuffer outputBuffer;

         while (itCurTransform != itLastTransform) {
            outputBuffer.clear();

            readBytes = this->processS2CTransform(inputBuffer, *(*itCurTransform),
               outputBuffer);

            inputBuffer.copy(outputBuffer);

            if (itCurTransform == itFirstTransform) {
               totalReadBytes = readBytes;
            }

            itCurTransform++;
         }

         if (outputBuffer.size() > 0) {
            //Discard the data read from the main buffer
            CBuffer tmpBuf = CBuffer(this->m_s2cReadBuffer);
            tmpBuf.pop_front(totalReadBytes);
            this->m_s2cReadBuffer = tmpBuf.detach();

            //Send result of transformations to client
            this->WriteToClient(outputBuffer);

            if (log_mode) {
               this->S2Cdump << "<-- " << std::time(nullptr) << " " << totalReadBytes << " " << 
                  outputBuffer.size() << " " << std::flush;
               boost::crc_32_type outputCRC;
               outputCRC.process_bytes(outputBuffer.data(), outputBuffer.size());
               this->S2Cdump << outputCRC.checksum() << std::endl << std::flush;
               this->S2Cdump.write(
                  reinterpret_cast<const char*>(outputBuffer.data()),
                  outputBuffer.size());
               this->S2Cdump << std::flush;
            }
         }
      }

      // If no error occured, re-setup the read handler
      if (!ec && !this->m_terminating) {
         CBuffer tmpBuf = CBuffer(this->m_s2cReadBuffer);

         // Need to reallocate ?
         if (tmpBuf.rightSpaceRemaining() < 1024) {

            if (tmpBuf.leftSpaceRemaining() < 1024) {
               tmpBuf.reserve(tmpBuf.capacity() + 1024);
            }

            tmpBuf.resetOffset(0);
         }

         this->m_s2cReadBuffer = tmpBuf.detach();
         this->PostS2CReadingRequest(this->m_s2cReadBuffer);

         // If eof (socket closed), don't re-setup the input handler
      } else if (ec == asio::error::eof) {

         if (!this->m_terminating) {
            this->m_terminating = true;

            CTunnel &tunnel = this->m_tunnel;
            tunnel.getIoService().post(
               [&] {
                  this->getTunnel().CloseEstablishedTunnel(this);
               }
            );
         }
      } else {
         // Remove this from establised connections list
         if (!this->m_terminating) {
            this->m_terminating = true;

            CTunnel &tunnel = this->m_tunnel;
            tunnel.getIoService().post(
               [&] {
                  this->getTunnel().CloseEstablishedTunnel(this);
               }
            );
         }
      }

   }

   /** Sends data to client.
    *  \param buf data to be sent
    */
   void CEstablishedTunnel::WriteToClient(CBuffer & buf) {
      boost::system::error_code ec;

      std::size_t dataWritten = asio::write(
         this->m_inputSocket,
         asio::buffer(buf.data(), buf.size()),
         asio::transfer_all(),
         ec);
   }

} //TuneHell namespace