#pragma once

namespace TuneHell 
{

   /** Holds the definition of one redirection list of data transformations, from local port to 
    *  remote host:port. A tunnel can manage several active instances of the redirection, one
    *  instance per established, held as a TuneHell::CEstablishedTunnel object.
    */
   class CTunnel
   {

      private:
         asio::io_service m_ioService;                   /**< boost::asio service */
         tcp::resolver m_resolver;                       /**< boost::asio resolver */
         tcp::resolver::query m_target_resolution_query; /**< boost::asio dns query object */
         tcp::endpoint m_listening_endpoint;             /**< boost::asio listen endpoint 
                                                              (i.e. socket) */
         tcp::acceptor m_listener;                       /**< boost::asio listener object */

         /** Temporary pointer ready to recieve incoming connection to be redirected. Ownership of 
          *  this pointer will be transfered to the newly created CEstablishedTunnel, stored in
          *  m_establishedTunnels
          */
         std::unique_ptr<CEstablishedTunnel> m_newTunnel;
         

         /** List of data transformation that will be applied to redirected data stream */
         std::list< std::unique_ptr<CDataTransform> > m_DataTransforms;

         /** List of established redirections */
         std::list< std::unique_ptr<CEstablishedTunnel> > m_establishedTunnels;

         tPort m_localPort;         /**< Local TCP port on which the redirection listens */
         std::string m_remoteHost;  /**< Remote host to which incoming data will be redirected */
         tPort m_remotePort;        /**< Remote TCP port on which incoming data will be redirected */

         void OnAccept(const boost::system::error_code& ec);

      public:
         CTunnel(tPort localPort, std::string targetHost, tPort targetPort);

         CTunnel(CTunnel &) = delete;
         CTunnel(CTunnel const &) = delete;
         CTunnel &operator=(CTunnel const &) = delete;
         CTunnel &operator=(CTunnel &) = delete;

         virtual ~CTunnel() {}

         void addTransformation(std::unique_ptr<TuneHell::CDataTransform>);
         std::list< std::unique_ptr<CDataTransform> > & getTransformations();
         void StartListening();
         
         void CloseEstablishedTunnel(CEstablishedTunnel *);

         asio::io_service &getIoService();

         tPort getLocalPort();
         tPort getRemotePort();
         std::string getRemoteHost();

         void Run();

   };

} //TuneHell namespace