#pragma once

namespace TuneHell
{
   class CTunnel;

   /** Holds an redirection that has an established incoming connection and an established outgoing
    *    connection. It has a list of data transformations that is applied to data passing through
    *    this tunnel.
    */
   class CEstablishedTunnel
   {
      private:
         tcp::socket m_inputSocket;                /**< Socket recieving client data */
         tcp::socket m_outputSocket;               /**< Socket sending transformed data to server */
      
         CTunnel &m_tunnel;                        /**< Redirection definition on which this 
                                                        established tunnel is based */
         bool m_terminating;                       /**< Boolean used to know if this tunnel is 
                                                        closing, and thus shouldn't accept any 
                                                        additional activity */

         CBuffer::tYieldBuffer m_c2sReadBuffer;    /**< Client to Server incoming data buffer */
         CBuffer::tYieldBuffer m_s2cReadBuffer;    /**< Server to Client incoming data buffer */

      
         std::list< std::unique_ptr<TuneHell::CDataTransform> > m_transforms; /**< List of data
               transformations applied on data when passing through this tunnel */

            //since TuneHell::CDataTransform is abstract pure, and we want to store inherited class
            //objects, can't be storage by value. Can it ?

         std::ofstream S2Cdump;                    /**< Debug output stream for Server to Client data */
         std::ofstream C2Sdump;                    /**< Debug output stream for Client to Server data */


         std::size_t processC2STransform(CBuffer &inputBuffer,
            CDataTransform &transform, CBuffer &outputBuffer);

         std::size_t processS2CTransform(CBuffer &inputBuffer,
            CDataTransform &transform, CBuffer &outputBuffer);

         void PostC2SReadingRequest(CBuffer::tYieldBuffer & buffer);
         void PostS2CReadingRequest(CBuffer::tYieldBuffer & buffer);

         void OnReadFromInput(const boost::system::error_code &ec, std::size_t bytes_transferred);
         void OnReadFromTarget(const boost::system::error_code &ec, std::size_t bytes_transferred);

         void WriteToTarget(CBuffer &);
         void WriteToClient(CBuffer &);

      public:
         CEstablishedTunnel(CTunnel &tunnel, 
            std::list< std::unique_ptr<TuneHell::CDataTransform> > &transforms);

         CEstablishedTunnel(CEstablishedTunnel const &) = delete;
         CEstablishedTunnel& operator=(CEstablishedTunnel&) = delete;

         virtual ~CEstablishedTunnel();

         tcp::socket &getInputSocket();
         tcp::socket &getOutputSocket();
         CTunnel &getTunnel();

         void StartReading();
   };

} //TuneHell namespace
