#pragma once

//Ansi C
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <ctime>

//STL
#include <string>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <algorithm>
#include <thread>
#include <functional>
#include <iterator>
#include <list>
#include <fstream>
#include <memory>
#include <utility>

//Boost
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/program_options.hpp>
#include <boost/crc.hpp>

//Python
#ifdef _DEBUG
#  undef _DEBUG
#  include <Python.h>
#  define _DEBUG
#else
#  include <Python.h>
#endif


namespace asio = boost::asio;
namespace ip = boost::asio::ip;
typedef boost::asio::ip::tcp tcp;

//Cryptography
#define CRYPTOPP_DEFAULT_NO_DLL
#undef CRYPTOPP_DLL_ONLY
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1

#include <cryptopp/dll.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/base64.h>

//Globals
namespace TuneHell {
   typedef unsigned short tPort;
   extern bool verbose_mode;
   extern bool log_mode;
}

//TuneHell includes
#include "buffer.hpp"
#include "datatransfom.hpp"
#include "establishedtunnel.hpp"
#include "tunnel.hpp"

//Data transformations
#include "transforms/html.hpp"
#include "transforms/caesar.hpp"
#include "transforms/vigenere.hpp"
#include "transforms/http.hpp"
#include "transforms/base64.hpp"
#include "transforms/python.hpp"
#include "transforms/symetriccrypt.hpp"


