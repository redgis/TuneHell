#pragma once

namespace TuneHell
{

   /** Transformation that can be applied to data buffer.
    */
   class CDataTransform
   {
      private:
         bool m_isReverse;    /**< Boolean specifying if the transformation is reverse. */

      public:

         /** Constructs a data transformation.
          */
         CDataTransform () : m_isReverse(false) {}


         /** Transforms data going from client to server.
          *  \param buffer data to be transformed
          *  \return amount of bytes consumed from the input buffer, actually read to be then 
          *    transformed
          */
         virtual std::size_t C2STransform(CBuffer &buffer) = 0;

         /** Reverse transforms data going from client to server.
          *  \param buffer data to be transformed
          *  \return amount of bytes consumed from the input buffer, actually read to be then
          *    transformed
          */
         virtual std::size_t C2SUnTransform(CBuffer &buffer) = 0;


         /** Transforms data going from server to client. By default, this calls 
          *    CDataTransform::C2STransform, thus making the transformation symetric between 
          *    client to server, and server to client.
          *  \param buffer data to be transformed
          *  \return amount of bytes consumed from the input buffer, actually read to be then 
          *    transformed
          */
         virtual std::size_t S2CTransform(CBuffer & buffer) 
            { return this->C2STransform(buffer); }

         /** Reverse transforms data going from server to client. By default, this calls 
          *    CDataTransform::C2SUnTransform, thus making the transformation symetric between 
          *    client to server, and server to client.
          *  \param buffer data to be transformed
          *  \return amount of bytes consumed from the input buffer, actually read to be then 
          *    transformed
          */
         virtual std::size_t S2CUnTransform(CBuffer & buffer)
            { return this->C2SUnTransform(buffer); }

         /** Says if the transformation is to be reversed, i.e. server to client is transformed, 
          *    and client to server is reverse transformed.
          *  \return true if the transformation is reversed, false otherwise
          */
         bool isReversed() const
            { return this->m_isReverse; }

         /** Specifies if the transformation is to be reversed, i.e. server to client is 
          *    transformed, and client to server is reverse transformed.
          *  \param isReverse true if the transformation should be reversed, false otherwise
          */
         void setReverseTransformation(bool isReverse) 
            { this->m_isReverse = isReverse; }

         /** Creates a duplicate instance of the transformation.
          *  \return unique pointer to the newly created instance
          */
         virtual std::unique_ptr<CDataTransform> clone() const = 0;
   };

} //TuneHell namespace