# ![](doc/logo.png) TuneHell - TCP forwarder

## Summary

This program allows one to redirect TCP communications from local port
to remote host/port. Data transformations can be applied when data is
recieved and before being forwarded.


## Parameters
    -h [ --help ]
        Prints help

    -d [ --debug ]
        Display connections activity output

    -l [ --log ]
        Write detailed debug output to file. Slow !

    -c [ --config-file ]
        Not implemented yet. Load parameters from a JSON config file

    -r [ --redirection ] arg 
        Space separated list of redirections defined as follows:
        [local port];[remote host];[remote port];{comma-separated list of transformations}

    Data transformations currently supported :
        html
        python(scriptname) : see [this example](doc/python_sample.py)
        caesar(key) : key is a integer < 255
        vigenere(key) : key is a string
        base64
        aes(keyfile)
        trippledes(keyfile)
        rsa(keyfile)*
     Protocole encapsulation:
        http
        https*
        dns*

    Transformation is defined from client to server, and must be prefixed with '+' for normal transformation, '-' for reverse transformation.
    * are not implemented yet

Example, on local machine, encrypting with caesar cipher and encapsulating in HTML, then redirecting to *remote.host.eg*:
> TuneHell --debug --redirection 7000;remote.host.eg;7001;+caesar(100),+vigenere(bonjour),+html

Example, on remote machine,decapsulating from HTML and decrypting with caesar cipher, redirecting to local 22 port:
> TuneHell --debug --redirection 7001;localhost;22;-html,-vigenere(bonjour),-caesar(100)

It is possible to define several redirections on one command line (i.e. on the same machine). In this example, the first redirection has no data transformation.
> TuneHell --debug --redirection 7000;remote.host.eg;7001; --redirection 7001;localhost;22;+html,+vigenere(bonjour),+caesar(100)